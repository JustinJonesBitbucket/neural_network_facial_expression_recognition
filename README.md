This project trains a Neural Network to recognize Facial Expressions on the Kaggle Facial Recognition Dataset found at :

https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data?fer2013.tar.gz

-Files:
-ann.py trains the Neural Network and Outputs the trained parameters to the Classifier_Save Folder. 
-show_images.py loads the dataset and displays the image with the label
-ann_load_predict.py loads a dataset and parameters and predicts on the dataset.

-The neural network contains code using the tanh and relu activation functions. Gradient descent is implemented with the correct simultaneous update of weights.


