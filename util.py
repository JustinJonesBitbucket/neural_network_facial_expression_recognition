import numpy as np
import pandas as pd


def init_weight_and_bias(M1, M2): #M1 is input size, M2 is output size

	# Randomized Weight matrix M1xM2, using Gaussian normal; Make std dev the sqrt of fan in + fan out
	W = np.random.randn(M1, M2) / np.sqrt(M1 + M2) 
	b = np.zeros(M2) # bias initialized as zeros of size output
	return W.astype(np.float32), b.astype(np.float32) # use float 32 for use with Theano and Tensorflow


def init_filter(shape, poolsz): # used with convolutional neural networks
	# shape is a tuple with 4 different values, divide by something like the fan in plus fanout
	w = np.random.randn(*shape) / np.sqrt(np.prod(shape[1:]) + shape[0]*np.prod(shape[2:] / np.prod(poolsz)))
	return w.astype(np.float32) # convert to float 32 for use in tensorflow and Theano


def relu(x): 
	# rectifier linear unit used as an activation function in the neural network
	#can be used with old Theano units without a built in relu
	return x * (x > 0)


def sigmoid(A): # sigmoid activiation function
    return 1 / (1 + np.exp(-A))


def softmax(A): # softmax activation function
    expA = np.exp(A)
    return expA / expA.sum(axis=1, keepdims=True)


def sigmoid_cost(T, Y): # calculates the cross entropy from sigmoid (binary classification)
    return -(T*np.log(Y) + (1-T)*np.log(1-Y)).sum()


def cost(T, Y): # more general cross entropy function will work for softmax
    return -(T*np.log(Y)).sum()


def cost2(T, Y): # calculates cross entrop, but uses the Targets instead of indicator matrix
    # same as cost(), just uses the targets to index Y
    # instead of multiplying by a large indicator matrix with mostly 0s
    N = len(T)
    return -np.log(Y[np.arange(N), T]).mean()


def error_rate(targets, predictions): # gives error rate between the targets and the predictions
    return np.mean(targets != predictions)


def y2indicator(y): # turns N x 1 Vector matrix of targets into N x K indicator matrix (one hot encoded)
    N = len(y)
    K = len(set(y))
    ind = np.zeros((N, K))
    for i in xrange(N):
        ind[i, y[i]] = 1
    return ind


def getData(balance_ones=True): # get all the data from all the classes
    # images are 48x48 = 2304 size vectors
    # N = 35887
    Y = []
    X = []
    first = True
    for line in open('fer2013.csv'):
        if first: # skip the first line as this is the header with column names in the file
            first = False
        else:
            row = line.split(',')
            Y.append(int(row[0])) # first column is the label
            X.append([int(p) for p in row[1].split()]) # second column is space separated pixel values

    X, Y = np.array(X) / 255.0, np.array(Y) # convert to numpy array, normalize the data so pixel values btwn 0-1

    if balance_ones: # this is due to the class imbalance problem.. we correct by repeating class1 9 times to get equal
		# balance the 1 class
		# take all the data not class X1 and sick into Variables X0, Y0
		X0, Y0 = X[Y!=1, :], Y[Y!=1]
		# set X1 to be the samples where Y==1
		X1 = X[Y==1, :]
		# repeat X1 9 times
		X1 = np.repeat(X1, 9, axis=0)
		# stack X0 and X1 back together
		X = np.vstack([X0, X1])
		# Then do the same for Y0 and Y1
		Y = np.concatenate((Y0, [1]*len(X1)))

    return X, Y


def getImageData(): # for use with convolutional neural networks, keeps original image shape
    X, Y = getData()
    N, D = X.shape
    d = int(np.sqrt(D))
    X = X.reshape(N, 1, d, d) # N samples, 1 color channel, width d, and height d (48 x 48 in this case)
    return X, Y


def getBinaryData(): # almost same thing as get regular data, except only add the samples for which class is 0 or 1
	# not dealing with class imbalance here
	Y = []
	X = []
	first = True
	for line in open('fer2013.csv'):
		if first:
		    first = False
		else:
		    row = line.split(',')
		    y = int(row[0])
		    if y == 0 or y == 1:
		        Y.append(y)
		        X.append([int(p) for p in row[1].split()])
	return np.array(X) / 255.0, np.array(Y)


def crossValidation(model, X, Y, K=5):
    # split data into K parts
    X, Y = shuffle(X, Y)
    sz = len(Y) / K
    errors = []
    for k in xrange(K):
        xtr = np.concatenate([ X[:k*sz, :], X[(k*sz + sz):, :] ])
        ytr = np.concatenate([ Y[:k*sz], Y[(k*sz + sz):] ])
        xte = X[k*sz:(k*sz + sz), :]
        yte = Y[k*sz:(k*sz + sz)]

        model.fit(xtr, ytr)
        err = model.score(xte, yte)
        errors.append(err)
    print "errors:", errors
    return np.mean(errors)
