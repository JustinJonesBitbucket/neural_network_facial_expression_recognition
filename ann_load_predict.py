import numpy as np
import matplotlib.pyplot as plt

from util import getData, softmax, cost2, y2indicator, error_rate, relu # uses cost2, with cost2 more efficient
from sklearn.utils import shuffle


class ANN(object):
	def __init__(self, M): # M is the number of hidden units in the hidden layer
		self.M = M
		
	def load(self):
		W2File = "./Classifier_Save/W2.npy"
		b2File = "./Classifier_Save/b2.npy"
		W1File = "./Classifier_Save/W1.npy"
		b1File = "./Classifier_Save/b1.npy"
		self.W2 = np.load(W2File)
		self.b2 = np.load(b2File)
		self.W1 = np.load(W1File)
		self.b1 = np.load(b1File)
	
	def forward(self, X):
		# Z = relu(X.dot(self.W1) + self.b1)
		Z = np.tanh(X.dot(self.W1) + self.b1)
		return softmax(Z.dot(self.W2) + self.b2), Z

	def predict(self, X):
		pY, _ = self.forward(X)
		return np.argmax(pY, axis=1)

	def score(self, X, Y):
		prediction = self.predict(X)
		return 1 - error_rate(Y, prediction) # this is the accuracy


def main():
	print "Loading Data.."
	X, Y = getData()
	model = ANN(200)
	print "Neural Network created"
	model.load()
	print "Loaded parameters"
	print "Final Score: ", model.score(X, Y)
	# scores = cross_val_score(model, X, Y, cv=5)
	# print "score mean:", np.mean(scores), "stdev:", np.std(scores)

if __name__ == '__main__':
    main()
